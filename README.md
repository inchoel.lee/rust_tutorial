# rust_tutorial
To become Rustacean!

# Summary
- Rust는 오너쉽을 기반으로 메모리를 관리한다.
- 오너쉽을 가진 변수가 scope를 벗어나는 경우, Drop 함수가 자동으로 호출된다. 이 때 메모리 해제가 이루어진다.
- 하나의 값(메모리 공간)의 오너쉽은 하나의 변수만 가질 수 있으므로 중복 해제 에러가 발생하지 않는다.
- 오너쉽은 String 타입과 같이 할당 받을 메모리의 크기가 정해져 있지 않은 타입들에 대해서만 적용된다.
- 함수 단위에서도 오너쉽은 같은 방식으로 동작한다.
- 변수의 값을 오너쉽의 이동없이 전달하고 싶다면 &를 변수명 앞에 붙여서 전달한다. (e.g. &str, &var)
- 위와 같이 오너쉽없이 값의 주소만 전달하는 것을 '대여(borrowing)'한다고 부른다.
- 대여한 값은 변경할 수 없다.
- 대여한 값을 변경하고 싶다면 해당 참조될 변수가 mut 으로 선언됐어야 하며 전달할 때도 &mut 을 붙여줘야 한다.
- &mut 이용한 대여는 한 scope에서 한번만 사용할 수 있다.
- 하나의 scope 내에서 immutable 대여(&)가 일어난 변수에 대해서 mutable 대여(&mut)를 할 수는 없다.
- Rust는 dangling reference 문제를 컴파일러가 체크한다.

- Keep in mind that Rust is a statically typed language - 컴파일 타임에 변수 타입을 결정될 수 있어야 한다.

# References
There are lots of information for Rust. I don't need to write them again.
- https://doc.rust-lang.org/1.30.0/book/second-edition/ch00-00-introduction.html
- https://medium.com/@kwoncharles/rust-%ED%94%84%EB%A1%9C%EA%B7%B8%EB%9E%98%EB%B0%8D-%EC%BB%A8%EC%85%89-a462e03bc9cd 
- https://medium.com/@kwoncharles/rust-%EB%9F%AC%EC%8A%A4%ED%8A%B8%EC%9D%98-%EA%BD%83-ownership-%ED%8C%8C%ED%97%A4%EC%B9%98%EA%B8%B0-2f9c6b744c38
- https://medium.com/@kwoncharles/rust-%EC%98%A4%EB%84%88%EC%8B%AD-%EC%B0%B8%EC%A1%B0-%EB%8C%80%EC%97%AC-16f825aaa882
- https://github.com/rust-lang/rust.vim
- https://github.com/neoclide/coc-rls
