fn main() {
    println!("Hello, world!");

    let x = another_function(5);
    println!("The value of x is: {}", x);
}

fn another_function(n: i32) -> i32 {
    println!("Another function!");

    n + 1
}
