fn main() {
    let mut x = 5;
    println!("The value of x is: {}", x);

    x = 6;
    println!("The value of x is: {}", x);

    // difference const vs let
    const MAX_POINTS: u32 = 100_000;

    // Shadowing
    let x = 5;
    let x = x + 1;
    let x = x + 2;
    println!("The value of x is {}", x);

    let spaces = "   ";
    let spaces = spaces.len();
    println!("The value of spaces is {}", spaces);

    // let mut spaces = "   ";
    // spaces = spaces.len();

    // Data type
    // let guess: = "42".parse().expect("Not a number!");
    let guess: u32 = "42".parse().expect("Not a number!");
    println!("The value of guess is {}", guess);

    // Scalar Types
    // integers, floating-point, numbers, Bolleans and characters.
    //
    // Compound Types
    // Tuple
    let tup: (i32, f64, u8) = (500, 6.4, 1);

    let (x, y, z) = tup;
    println!("The value of y is: {}", y);

    let five_hundred = tup.0;
    let six_point_four = tup.1;
    let one = tup.2;

    println!("The value of one is: {}", one);

    // Array
    let arr = [1, 2, 3, 4, 5];
    let third = arr[2];
    println!("The value of third is: {}", third);
}
